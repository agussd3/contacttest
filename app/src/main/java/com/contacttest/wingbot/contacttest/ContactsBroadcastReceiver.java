package com.contacttest.wingbot.contacttest;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

@SuppressLint("NewApi")
public class ContactsBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context,final Intent intent) {

//        if (!intent.getAction().equals("com.androidbook.intents.testbc")){
            this.sendNotification(context, "Boot Loaded");
            Intent newIntent = new Intent(context,ContactTestService.class);
            context.startService(newIntent);

    }
    private void sendNotification(Context ctx, String message) {
        //Get the notification manager
        String ns = Context.NOTIFICATION_SERVICE;
        int icon = R.drawable.abc_btn_radio_material;
        NotificationManager nm = (NotificationManager) ctx.getSystemService(ns);
        //Prepare Notification Object Details
        long when = System.currentTimeMillis();
        //Get the intent to fire when the notification is selected
        //Create the notification object through the builder
        Notification notification = new NotificationCompat.Builder(ctx)
                .setContentTitle("Intent Received")
                .setContentText(message)
                .setSmallIcon(icon)
                .setWhen(when)
                .setContentInfo(message)
                .build();
        //Send notification
        //The first argument is a unique id for this notification.
        //This id allows you to cancel the notification later
        //This id also allows you to update your notification
        //by creating a new notification and resending it against that id
        //This id is unique with in this application
        nm.notify(1, notification);





    }

}
