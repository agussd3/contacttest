package com.contacttest.wingbot.contacttest;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ContactTestService extends Service implements Runnable {
    private static int FOREGROUND_ID = 1;
    private final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private Thread workingThread;

    @Override
    public void onCreate(){

        sendNotification(this.getApplicationContext(), "Se creo el servicio");
        workingThread =  new Thread(this);
        workingThread.start();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void run() {

        int icon = R.drawable.abc_btn_radio_material;
        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle("Service changed to foreground")
                .setContentText("Service is running in foreground")
                .setSmallIcon(icon)
                .setWhen(System.currentTimeMillis())
                .setContentInfo("Service is running in foreground")
                .build();

        startForeground(FOREGROUND_ID, notification);
        boolean running = true;

        getApplicationContext().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true,
            new ContentObserver(null){
                    @Override
                    public void onChange(boolean selfChange, Uri uri){
                        Log.d("Contacto nuevo", uri.toString()  );
                        sendNotification(getApplicationContext(), "Contacto nuevo " + uri);
                    }

                }
        );


        while (running) {

            try {
                Thread.sleep(1000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
            sendNotification(this.getApplicationContext(), "Sigo vivo a las: " + formatter.format(new Date()));
        }
        sendNotification(this.getApplicationContext(), "Me mori a las: " + formatter.format(new Date()));
    }

    private void sendNotification(Context ctx, String message) {
        //Get the notification manager
        String ns = Context.NOTIFICATION_SERVICE;
        int icon = R.drawable.abc_btn_radio_material;
        NotificationManager nm = (NotificationManager) ctx.getSystemService(ns);
        //Prepare Notification Object Details
        long when = System.currentTimeMillis();
        //Get the intent to fire when the notification is selected
        //Create the notification object through the builder
        Notification notification = new NotificationCompat.Builder(ctx)
                .setContentTitle("Intent Received")
                .setContentText(message)
                .setSmallIcon(icon)
                .setWhen(when)
                .setContentInfo(message)
                .build();
        //Send notification
        //The first argument is a unique id for this notification.
        //This id allows you to cancel the notification later
        //This id also allows you to update your notification
        //by creating a new notification and resending it against that id
        //This id is unique with in this application
        nm.notify(1, notification);

    }

}


